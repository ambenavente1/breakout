package cs151.arcade.breakout;

import cs151.arcade.breakout.main.Breakout;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.Game;
import org.newdawn.slick.ScalableGame;
import org.newdawn.slick.SlickException;

/**
 * Entry point to breakout
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/17/14
 */
public class Start {
    /**
     * This is the entry point to the program. The program takes two
     * arguments, the first being the width of the scaled window and the
     * second being the height of the scaled window.
     *
     * @param args The width and then height separated by a space
     */
    public static void main(String[] args) {
        final int WINDOW_WIDTH;
        final int WINDOW_HEIGHT;

        if (args.length >= 2) {
            WINDOW_WIDTH  = Integer.parseInt(args[0]);
            WINDOW_HEIGHT = Integer.parseInt(args[1]);
        } else {
            WINDOW_WIDTH  = Breakout.GAME_WIDTH;
            WINDOW_HEIGHT = Breakout.GAME_HEIGHT;
        }

        Game game                   = new Breakout();
        ScalableGame scalableGame   = new ScalableGame(game,
                                                       WINDOW_WIDTH,
                                                       WINDOW_HEIGHT);
        try {
            AppGameContainer container = new AppGameContainer(scalableGame);
            container.setDisplayMode(WINDOW_WIDTH, WINDOW_HEIGHT, false);
            container.setTargetFrameRate(60);
            container.setShowFPS(false);
            container.start();
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }
}
