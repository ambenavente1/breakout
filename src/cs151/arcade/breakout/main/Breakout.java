package cs151.arcade.breakout.main;

import cs151.arcade.breakout.states.*;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

/**
 * The game in charge of running/switching game states.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/17/14
 */
public class Breakout extends StateBasedGame {

    /**
     * The width of the game area
     */
    public static final int GAME_WIDTH = 640;

    /**
     * The height of the game area
     */
    public static final int GAME_HEIGHT = 480;

    /**
     * Create a new instance of Breakout
     *
     */
    public Breakout() {
        super("Breakout!");
    }

    /**
     * Initialise the list of states making up this game
     *
     * @param container The container holding the game
     * @throws SlickException Indicates a failure to initialise the state
     * based game resources
     */
    @Override
    public void initStatesList(GameContainer container)
            throws SlickException {
        addState(new SplashState());
        addState(new MainMenuState(this));
        addState(new GameplayState());
        addState(new GameOverState());
        addState(new QuitState());
        enterState(States.MAIN_MENU.getID());
    }

}
