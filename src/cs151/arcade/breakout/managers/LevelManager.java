package cs151.arcade.breakout.managers;

import cs151.arcade.breakout.objects.bricks.BrickStructure;
import cs151.arcade.breakout.objects.bricks.BrickStructureReader;

import java.io.File;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.HashMap;

/**
 * TODO: Class description goes here
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/17/14
 */
public class LevelManager {

    private static final String[] DEFAULT_LEVELS = new String[] {
            "level_1.txt",
            "level_2.txt",
            "level_3.txt",
            "level_4.txt",
            "level_5.txt",
            "level_6.txt"
    };

    /**
     * The current level the game is at
     */
    private int currentLevel;

    /**
     * Holds all the levels and can be referenced by an ID
     */
    private HashMap<Integer, BrickStructure> levels;

    /**
     * If the player finished the final level
     */
    private boolean gameOver;

    /**
     * Creates a level manager instance that starts at level 1
     */
    public LevelManager() {
        this.currentLevel = 1;
        this.levels = new HashMap<Integer, BrickStructure>();
        this.gameOver = false;
        initDefaultLevels();
    }

    /**
     * Initializes the default levels found in the levels directory
     */
    private void initDefaultLevels() {
        BrickStructureReader reader = new BrickStructureReader();

        for (String level : DEFAULT_LEVELS) {
            BrickStructure structure = reader.readStructureFromStream(
                    getClass().getResourceAsStream(level)
            );
            levels.put(structure.getId(), structure);
        }
    }

    /**
     * Goes onto the next level if there is level to go to
     *
     * @throws java.lang.RuntimeException if there are no more levels.
     * This will be changed as soon as there is some gameplay functionality
     * with game over.
     */
    public void nextLevel() {
        if (currentLevel < levels.size()) {
            currentLevel++;
        } else {
            gameOver = true;
        }
    }

    /**
     * Gets the BrickStructure of the current level
     *
     * @return the BrickStructure of the current level
     */
    public BrickStructure getCurrentLevel() {
        return levels.get(currentLevel);
    }

    /**
     * Gets if every brick in the level is null or unbreakable
     *
     * @return if the structure is 'empty'
     */
    public boolean isEmpty() {
        return getCurrentLevel().isEmpty();
    }

    /**
     * Gets if all the levels have been beaten
     */
    public boolean isGameOver() {
        return gameOver;
    }
}
