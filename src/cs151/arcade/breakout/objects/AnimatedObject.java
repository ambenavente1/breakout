package cs151.arcade.breakout.objects;

import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.state.StateBasedGame;

/**
 * TODO: Class description goes here
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/17/14
 */
public abstract class AnimatedObject extends StaticObject {

    /**
     * The animation that represents this object.  This is similar to the
     * StaticObject's image, but instead, has multiple, loop-able frames.
     */
    private Animation animation;

    /**
     * Called to initialize the animation for this AnimatedObject that will
     * be drawn to the screen
     */
    protected abstract void initAnimation();

    /**
     * Draws an animation representing this object, to the screen.
     *
     * @param container The game container in charge of managing input and
     *                  handling the game loop
     * @param g         The graphics context used for rendering things onto
     */
    @Override
    public void render(GameContainer container,
                       StateBasedGame game,
                       Graphics g) {
        if (animation != null) {
            // TODO: Account for width, height, angle
            animation.draw(getX(), getY());
        } else {
            super.render(container, game, g);
        }
    }
}
