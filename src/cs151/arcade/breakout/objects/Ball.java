package cs151.arcade.breakout.objects;

import cs151.arcade.breakout.objects.interfaces.Collidable;
import cs151.arcade.breakout.objects.interfaces.Updatable;
import cs151.arcade.breakout.main.Breakout;
import cs151.arcade.breakout.sounds.SoundManager;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

/**
 * TODO: Class description goes here
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/17/14
 */
public class Ball extends StaticObject implements Updatable, Collidable {

    /**
     * The default speed that the ball moves in the y dimension
     */
    private static final float DEFAULT_Y_VEL = 7.4f;

    /**
     * The maximum speed the ball can move via its velocity's x component
     */
    private static final float TERMINAL_X_VEL = 7.5f;

    /**
     * Starting position gap between the ball and paddle
     */
    private static final int MARGIN_BOTTOM = 5;

    /**
     * The velocity vector for the Ball. This should be constant and should
     * only change when reflecting.
     */
    private final Vector2f vel;

    /**
     * Creates a ball object at a position
     *
     * @param pos The position to spawn the ball at
     */
    public Ball(Vector2f pos) {
        vel = new Vector2f(0, 0);

        initImage();

        setPos(pos);

        resetVel();
    }

    /**
     * Creates a ball object that spawns directly above the paddle
     */
    public Ball(Paddle paddle) {

        vel = new Vector2f(0, 0);

        initImage();

        // Set the position after dimensions have been set
        resetPos(paddle);

        // Sets the velocity to be randomized (not the Y component)
        resetVel();
    }

    /**
     * Initializes the Ball's image
     */
    @Override
    protected void initImage() {
        try {
            setImage(new Image("res/balls/ballBlue.png"));
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }

    /**
     * Updates the logic of this object
     *
     * @param container The game container in charge of managing input and
     *                  handling the game loop
     * @param game      The game in charge of managing the state system
     * @param delta     The amount of milliseconds that occurred from the
     */
    @Override
    public void update(GameContainer container,
                       StateBasedGame game,
                       int delta) {
        move(vel);
        checkBounds();

    }

    /**
     * Moves the ball a given amount and clamps the ball's position
     * within the game bounds (except for at the bottom)
     *
     * @param amount the amount to move the ball
     */
    private void move(Vector2f amount) {
        setPos(new Vector2f(getX() + amount.x, getY() + amount.y));
    }

    /**
     * Checks and adjusts if the ball goes out of bounds (except for the
     * bottom bounds)
     */
    private void checkBounds() {
        if (getY() < 0) {
            setPos(new Vector2f(getX(), 0));
            reflectY();
        }

        if (getX() < 0) {
            setPos(new Vector2f(0, getY()));
            reflectX();
        }

        if (getX() > Breakout.GAME_WIDTH - getWidth()) {
            setPos(new Vector2f(Breakout.GAME_WIDTH - getWidth(), getY()));
            reflectX();
        }
    }

    /**
     * Reflects the x component of the velocity vector
     */
    public void reflectX() {
        vel.x *= -1;
        onCollide();
    }

    /**
     * Reflects the y component of the velocity vector
     */
    public void reflectY() {
        vel.y *= -1;
        onCollide();
    }

    /**
     * Gets a copy of the ball's velocity vector
     *
     * @return the ball's velocity vector
     */
    public Vector2f getVel() {
        return vel.copy();
    }

    /**
     * Adds velocity to the ball's velocity
     *
     * @param amount the amount of velocity to add
     */
    public void addVel(Vector2f amount) {
        vel.add(amount);
        vel.x = vel.x >  TERMINAL_X_VEL ?  TERMINAL_X_VEL :
                vel.x < -TERMINAL_X_VEL ? -TERMINAL_X_VEL :
                vel.x;
    }

    /**
     * Resets the ball's position to be directly above the paddle
     *
     * @param paddle the main game paddle
     */
    public void resetPos(Paddle paddle) {
        setPos(new Vector2f(paddle.getX() + paddle.getWidth() / 2 -
                getWidth() / 2, paddle.getY() - getHeight() - MARGIN_BOTTOM));
    }

    /**
     * Resets the ball's velocity to be going upward and to have a random x
     * component
     */
    void resetVel() {
        vel.x = random.nextFloat(-5.5f, 5.5f);
        vel.y = DEFAULT_Y_VEL;
    }


    /**
     * When the ball collides something, it creates a pop sound
     */
    @Override
    public void onCollide() {
        SoundManager.POP.play();
    }
}
