package cs151.arcade.breakout.objects;

import cs151.arcade.breakout.util.AndRandom;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;

/**
 * TODO: Class description goes here
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/17/14
 */
public class GameObject {

    /**
     * Random number generator for use with all GameObjects
     */
    protected static final AndRandom random = new AndRandom();

    /**
     * The position of this object in the game world
     */
    private Vector2f pos;

    /**
     * The width of this object in pixels
     */
    private int width;

    /**
     * The height of this object in pixels
     */
    private int height;

    /**
     * The bounding box representing the collision area of this game object
     */
    private Rectangle bounds;

    /**
     * Initializes the game object at a default position of [0, 0]
     * and default dimensions of 0 (width), and (0) height.
     */
    public GameObject() {
        this(new Vector2f(0, 0));
    }

    /**
     * Initializes the game object at a specific location and default
     * dimensions of 0 (width) and 0 (height).
     *
     * @param pos The position to initialize this object at
     */
    public GameObject(Vector2f pos) {
        this.pos = pos;
        this.width = 0;
        this.height = 0;
        bounds = new Rectangle(pos.x, pos.y, width, height);
    }

    /**
     * Gets the x position of this GameObject in the game world
     *
     * @return The x coordinate of this GameObject's vector position
     */
    public float getX() {
        return pos.x;
    }

    /**
     * Gets the y position of this GameObject in the game world
     *
     * @return The y coordinate of this GameObject's vector position
     */
    public float getY() {
        return pos.y;
    }

    /**
     * Gets the position of the object in the game world
     *
     * @return a copy of the location of the object in the game world (by
     * pixel on screen)
     */
    public Vector2f getPos() {
        return pos.copy();
    }

    /**
     * Sets the position of the object in the game world
     *
     * @param pos The new position to set the object at
     */
    protected void setPos(Vector2f pos) {
        this.pos = pos;
        updateBounds();
    }

    /**
     * Gets the width of the game object in pixels
     *
     * @return The width of the game object in pixels
     */
    public int getWidth() {
        return width;
    }

    /**
     * Sets the width of the game object in pixels
     *
     * @param width The new width of the game object in pixels
     */
    protected void setWidth(int width) {
        this.width = width;
        updateBounds();
    }

    /**
     * Gets the height of the game object in pixels
     *
     * @return The height of the game object in pixels
     */
    public int getHeight() {
        return height;
    }

    /**
     * Sets the height of the game object in pixels
     *
     * @param height The new height of the game object in pixels
     */
    protected void setHeight(int height) {
        this.height = height;
        updateBounds();
    }

    /**
     * Gets the bounding box of this GameObject
     *
     * @return A rectangle representing the bounding box of this GameObject
     */
    public Rectangle getBounds() {
        return bounds;
    }

    /**
     * Updates the bounding box of this GameObject every time its position,
     * width, or height changes
     */
    private void updateBounds() {
        bounds.setBounds(pos.x, pos.y, width, height);
    }

    public Vector2f centerPos() {
        return new Vector2f(pos.x + width / 2, pos.y + height / 2);
    }
}
