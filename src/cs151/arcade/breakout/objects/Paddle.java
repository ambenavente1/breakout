package cs151.arcade.breakout.objects;

import cs151.arcade.breakout.objects.interfaces.Updatable;
import cs151.arcade.breakout.main.Breakout;
import org.newdawn.slick.*;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

/**
 * TODO: Class description goes here.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/17/14
 */
public class Paddle extends StaticObject implements Updatable {

    /**
     * The default speed the paddle moves left and right
     */
    private static final float X_SPEED = 9.4f;

    /**
     * Number of pixels from the bottom that the paddle's bottom is located
     */
    private static final int BOTTOM_MARGIN = 5;

    /**
     * The rate at which the paddle's position is changing
     */
    private Vector2f vel;

    /**
     * Number of times the player can die
     */
    private int lives;

    /**
     * If the player has run out of lives
     */
    private boolean gameOver;

    /**
     * Initializes a Paddle object
     */
    public Paddle() {
        initImage();
        setPos(new Vector2f(Breakout.GAME_WIDTH / 2 - getWidth() / 2,
                Breakout.GAME_HEIGHT  - getHeight() - BOTTOM_MARGIN));
        lives = 3;
        gameOver = false;
    }

    /**
     * Initializes the paddle's image.
     */
    @Override
    protected void initImage() {
        try {
            setImage(new Image("res/paddles/paddleBlu.png"));
        } catch (SlickException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Updates the logic of this object
     *
     * @param container The game container in charge of managing input and
     *                  handling the game loop
     * @param game      The game in charge of managing the state system
     * @param delta     The amount of milliseconds that occurred from the
     */
    @Override
    public void update(GameContainer container,
                       StateBasedGame game,
                       int delta) {
        if (!gameOver) {
            Input input = container.getInput();
            checkForInput(input);
        }
    }

    /**
     * Checks for player input
     *
     * @param input the input manager called from the game container
     */
    private void checkForInput(Input input) {
        if (input.isKeyDown(Input.KEY_LEFT)) {
            move(-X_SPEED);
        }

        if (input.isKeyDown(Input.KEY_RIGHT)) {
            move(X_SPEED);
        }

        if (!input.isKeyDown(Input.KEY_RIGHT) &&
            !input.isKeyDown(Input.KEY_LEFT)) {
            move(0);
        }
    }

    /**
     * Overrides super.render because the player requires that its number
     * of lives are shown on the screen
     *
     * @param container The game container in charge of managing input and
     *                  handling the game loop
     * @param g         The graphics context used for rendering things onto
     */
    @Override
    public void render(GameContainer container,
                       StateBasedGame game,
                       Graphics g) {
        super.render(container, game, g);

        Font gFont = g.getFont();
        g.drawString("Lives: " + lives,
                     Breakout.GAME_WIDTH - gFont.getWidth("Lives: " +
                             lives) - 5,
                     Breakout.GAME_HEIGHT - gFont.getLineHeight() - 5);
    }

    /**
     * Moves the paddle a given amount and clamps the paddle's position
     * within the game bounds
     *
     * @param amount the amount to move the game paddle
     */
    private void move(float amount) {
        Vector2f v = new Vector2f(amount, 0);

        vel = v;
        setPos(new Vector2f(getX() + v.x, getY()));
        if (getX() < 0) {
            setPos(new Vector2f(0, getY()));
        } else if (getX() + getWidth() > Breakout.GAME_WIDTH) {
            setPos(new Vector2f(Breakout.GAME_WIDTH - getWidth(), getY()));
        }
    }

    /**
     * Gets the paddle's vector velocity
     *
     * @return the velocity of the paddle
     */
    public Vector2f getVel() {
        return vel.copy();
    }

    /**
     * Gets if the player has run out of lives and therefore has reached,
     * "Game Over"
     *
     * @return if the player has run out of lives
     */
    public boolean isGameOver() {
        return gameOver;
    }

    /**
     * Takes away one of the player's lives
     */
    public void deductLife() {
        gameOver = --lives <= 0;
    }

    /**
     * Increases the player's life by one
     */
    public void incrementLife() {
        lives++;
    }
}
