package cs151.arcade.breakout.objects;

import org.newdawn.slick.*;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;
import cs151.arcade.breakout.objects.interfaces.Renderable;

/**
 * TODO: Class description goes here
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/17/14
 */
public abstract class StaticObject extends GameObject implements Renderable {

    /**
     * The image representing this object that will be rendered to the
     * screen. If this is left <code>null</code>, the render method will
     * just draw the bounding box of this object.
     */
    private Image image;

    public StaticObject() {
        this(new Vector2f(0, 0));
    }

    public StaticObject(Vector2f vector2f) {
        super(vector2f);
    }

    /**
     * Called to initialize the image of this StaticObject
     */
    protected abstract void initImage();

    /**
     * Draws a single image representing this object, to the screen.
     *
     * @param container The game container in charge of managing input and
     *                  handling the game loop
     * @param g         The graphics context used for rendering things onto
     */
    @Override
    public void render(GameContainer container,
                       StateBasedGame game,
                       Graphics g) {
        if (image != null) {
            image.draw(getX(), getY());
        } else {
            g.draw(getBounds());
        }
    }

    /**
     * Gets the texture that is drawn to the screen for this StaticObject
     *
     * @return This StaticObject's texture
     */
    public Image getImage() {
        return image;
    }

    /**
     * Sets the image for this GameObject. This method also sets the width
     * and height to default to the dimensions of the image.
     *
     * @param image The image to be used to draw this object to the screen
     */
    protected void setImage(Image image) {
        this.image = image;
        setWidth(image.getWidth());
        setHeight(image.getHeight());
    }
}
