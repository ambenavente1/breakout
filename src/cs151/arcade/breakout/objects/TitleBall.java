package cs151.arcade.breakout.objects;

import cs151.arcade.breakout.main.Breakout;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

/**
 * TODO: Class description goes here.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/22/14
 */
public class TitleBall extends Ball {

    /**
     * Creates a ball object for the title screen
     */
    public TitleBall(Vector2f pos) {
        super(pos);
    }

    @Override
    public void update(GameContainer container, StateBasedGame game, int delta) {
        super.update(container, game, delta);

        if (getY() + getHeight() > Breakout.GAME_HEIGHT) {
            setPos(new Vector2f(getX(), Breakout.GAME_HEIGHT - getHeight()));
            reflectY();
        }
    }

    public void setRandPos() {
        setPos(new Vector2f(random.nextInt(Breakout.GAME_WIDTH - getWidth()),
                random.nextInt(Breakout.GAME_HEIGHT - getHeight())));
    }
}
