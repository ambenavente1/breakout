package cs151.arcade.breakout.objects.bricks;

import cs151.arcade.breakout.objects.interfaces.Collidable;
import cs151.arcade.breakout.objects.StaticObject;
import cs151.arcade.breakout.sounds.SoundManager;
import org.newdawn.slick.*;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

/**
 * TODO: Class description goes here
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/17/14
 */
public class Brick extends StaticObject implements Collidable {

    /**
     * Images for the crack overlays
     */
    private static final Image[] cracks = new Image[3];

    /**
     * Initializes the crack images. I need a static statement because
     * creating a new Slick2D image requires catching a SlickException
     */
    static {
        try {
            cracks[0] = new Image("res/bricks/crack_0.png");
            cracks[1] = new Image("res/bricks/crack_1.png");
            cracks[2] = new Image("res/bricks/crack_2.png");
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }

    /**
     * The number that represents a brick is not damaged
     */
    private static final int NO_DAMAGE = -1;

    /**
     * Levels for brick strength (amount of times to hit until it dies - 1)
     */

    /**
     * Level of brick strength
     */
    private BrickStrength level;

    /**
     * The amount of damage this brick has taken
     */
    private int damageLevel;

    /**
     * If this brick is dead or not
     */
    private boolean dead;

    /**
     * Initializes this brick at a position
     *
     * @param pos The position to place this brick at
     */
    public Brick(Vector2f pos) {

        // TODO: Make the brick's type dictated by a BrickStructure data-type
        level       = BrickStrength.FOUR;
        damageLevel = NO_DAMAGE;
        dead        = false;

        setPos(pos);
        initImage();

    }

    /**
     * Called to initialize the image of this StaticObject. Note: be sure
     * to call this in the constructor of the child using it.
     */
    @Override
    protected void initImage() {
        // TODO: Special image system for the bricks
        try {
            setImage(new Image("res/bricks/brick_grey.png"));
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }

    /**
     * Knocks the strength of the brick down one level
     */
    @Override
    public void onCollide() {
        if (level != BrickStrength.UNBREAKABLE) {
            SoundManager.BREAK.play(1.0f, 1.5f);
            damageLevel++;
            level = BrickStrength.values()[level.ordinal() - 1];
            dead = level == BrickStrength.ZERO;
        }
    }

    /**
     * Draws a single image representing this object, to the screen.
     *
     * @param container The game container in charge of managing input and
     *                  handling the game loop
     * @param g         The graphics context used for rendering things onto
     */
    @Override
    public void render(GameContainer container,
                       StateBasedGame game,
                       Graphics g) {
        //noinspection StatementWithEmptyBody
        if (dead) {
            // TODO: Draw the brick explosion animation here
        } else {
            super.render(container, game, g);

            // Draws the cracks on top of the bricks
            if (damageLevel > NO_DAMAGE && damageLevel < cracks.length) {
                cracks[damageLevel].draw(getX(), getY());
            }
        }
    }

    /**
     * Gets the level of brick strength for this brick
     *
     * @return the level of brick strength
     */
    public BrickStrength getLevel() {
        return level;
    }

    /**
     * Sets the level of brick strength for this brick
     *
     * @param level the new level of brick strength
     */
    public void setLevel(BrickStrength level) {
        this.level = level;
        Color brickColor = level.getColor();
        getImage().setImageColor(brickColor.r, brickColor.g, brickColor.b);
    }

    /**
     * Gets if the brick is dead or not (i.e. if the brick's health has
     * reached or gone lower than 0)
     *
     * @return if the brick is dead or not
     */
    public boolean isDead() {
        return dead;
    }

    /**
     * Gets if two bricks are equal to each other (not by reference address)
     *
     * @param o the brick to compare to
     * @return if the two bricks are equal
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Brick brick = (Brick) o;

        return  damageLevel == brick.damageLevel    &&
                dead        == brick.dead           &&
                level       == brick.level          &&
                getWidth()  == brick.getWidth()     &&
                getHeight() == brick.getHeight()    &&
                getPos().equals(brick.getPos())     &&
                getImage().equals(brick.getImage());
    }

    /**
     * Gets the hash code for this brick
     *
     * @return a unique hash code for this brick object
     */
    @Override
    public int hashCode() {
        int result = level != null ? level.hashCode() : 0;
        result = 31 * result + damageLevel;
        result = 31 * result + (dead ? 1 : 0);
        return result;
    }
}
