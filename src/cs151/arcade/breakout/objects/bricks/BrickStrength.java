package cs151.arcade.breakout.objects.bricks;

import org.newdawn.slick.Color;

/**
 * TODO: Class description goes here.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/17/14
 */
public enum BrickStrength {
    /**
     * This brick is a dead brick
     */
    ZERO(0, 0, 0),

    /**
     * This brick will die in one more hit
     */
    ONE(255, 0, 0),

    /**
     * This brick will die in two more hits
     */
    TWO(255, 128, 0),

    /**
     * This brick will die in three more hits
     */
    THREE(255, 255, 0),

    /**
     * This brick will die in four more hits
     */
    FOUR(128, 255, 0),

    /**
     * This brick will never die
     */
    UNBREAKABLE(255, 255, 255);

    /**
     * The color overlay of the brick
     */
    private final Color color;

    /**
     * Creates a new strength of brick with the given color
     *
     * @param r the red value of the color
     * @param g the green value of the color
     * @param b the blue value of the color
     */
    private BrickStrength(int r, int g, int b) {
        color = new Color(r, g, b);
    }

    /**
     * Gets the color overlay for this strength of brick
     *
     * @return the color overlay for the brick strength
     */
    public Color getColor() {
        return color;
    }
}
