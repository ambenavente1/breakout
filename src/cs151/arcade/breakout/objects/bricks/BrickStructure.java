package cs151.arcade.breakout.objects.bricks;

import cs151.arcade.breakout.objects.Ball;
import cs151.arcade.breakout.objects.interfaces.Renderable;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.state.StateBasedGame;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO: Class description goes here
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/17/14
 */
public class BrickStructure implements Renderable {

    /**
     * Number representing if a level does not have an id
     */
    private static final int NO_ID = -1;

    /**
     * The maximum amount of bricks wide the structure can be
     */
    private static final int MAX_WIDTH = 10;

    /**
     * The maximum amount of bricks high the structure can be
     */
    public static final int MAX_HEIGHT = 12;

    /**
     * The default name given to BrickStructure objects with no name
     * specified
     */
    private static final String DEFAULT_NAME = "Brick Structure";

    /**
     * The ID for this BrickStructure
     */
    private int id;

    /**
     * The array containing the bricks of the structure
     */
    private Brick[][] bricks;

    /**
     * The name of this level
     */
    private String name;

    /**
     * Creates a BrickStructure with a default id of NO_ID and a default
     * name of "Brick Structure"
     */
    public BrickStructure() {
        this.id = BrickStructure.NO_ID;
        this.name = DEFAULT_NAME;
        this.bricks = new Brick[MAX_HEIGHT][MAX_WIDTH];
    }

    /**
     * Sets the brick at the given position
     *
     * @param x     The x coordinate to set the brick at
     * @param y     The y coordinate to set the brick at
     * @param brick The brick to put there
     */
    public void setBrick(int x, int y, Brick brick) {
        bricks[y][x] = brick;
    }

    /**
     * Gets the brick at the given position
     *
     * @param x The x coordinate of the brick
     * @param y The y coordinate of the brick
     * @return The brick at the specified coordinates
     */
    public Brick getBrick(int x, int y) {
        return bricks[y][x];
    }

    /**
     * Gets the name of this BrickStructure
     *
     * @return the name of this BrickStructure
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of this BrickStructure
     *
     * @param name the new name of this BrickStructure
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the id of this BrickStructure
     *
     * @return the id for this brick structure
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the id of this brick structure
     *
     * @param id the new id for this BrickStructure
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Where the logic for drawing this object lies.
     *
     * @param container The game container in charge of managing input and
     *                  handling the game loop
     * @param g         The graphics context used for rendering things onto
     */
    @Override
    public void render(GameContainer container,
                       StateBasedGame game,
                       Graphics g) {
        for (int y = 0; y < bricks.length; y++) {
            for (int x = 0; x < bricks[y].length; x++) {
                if (bricks[y][x] != null) {
                    if (bricks[y][x].isDead()) {
                        bricks[y][x] = null;
                    } else {
                        bricks[y][x].render(container, game, g);
                    }
                }
            }
        }
    }

    /**
     * Gets if the BrickStructure is empty (not including unbreakable
     * (silver) bricks)
     *
     * @return if the BrickStructure has no more bricks in it
     */
    public boolean isEmpty() {
        boolean result = true;
        for (int y = 0; result && y < bricks.length; y++) {
            for (int x = 0; result && x < bricks[y].length; x++) {
                Brick brick = bricks[y][x];
                result = brick == null ||
                        brick.getLevel() == BrickStrength.UNBREAKABLE;
            }
        }
        return result;
    }

    /**
     * Gets if two BrickStructure are equal to each other
     *
     * @param o The other BrickStructure to compare
     * @return If the two BrickStructures are equal
     */
    @Override
    public boolean equals(Object o) {
        boolean equals = true;
        if (getClass() == o.getClass()) {
            BrickStructure other = (BrickStructure) o;
            equals = name.equals(other.name);

            for (int y = 0; equals && y < bricks.length; y++) {
                for (int x = 0; equals && x < bricks[y].length; x++) {
                    equals = bricks[y][x].equals(other.bricks[y][x]);
                }
            }
        }
        return equals;
    }

    /**
     * Gets the hash code for this object
     *
     * @return the hash code representing this object
     */
    @Override
    public int hashCode() {
        return name.hashCode() + bricks.hashCode();
    }

    /**
     * Gets if the Ball passed in collides with any brick
     *
     * @param ball the ball to check for collisions with
     * @return the brick that is colliding with the ball
     */
    public List<Brick> collision(Ball ball) {
        // TODO: Better collision detection algorithm
        List<Brick> result = new ArrayList<Brick>();
        for (int y = 0; y < bricks.length; y++) {
            for (int x = 0; x < bricks[y].length; x++) {
                Brick brick = bricks[y][x];
                if (brick != null) {
                    if (ball.getBounds().intersects(brick.getBounds())) {
                        result.add(brick);
                    }
                }
            }
        }
        return result;
    }
}
