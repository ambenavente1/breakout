package cs151.arcade.breakout.objects.bricks;

import org.newdawn.slick.geom.Vector2f;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Scanner;

/**
 * TODO: Class description goes here
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/17/14
 */
public class BrickStructureReader {

    /**
     * Returns a BrickStructure read from a txt file
     *
     * @param path the file location of the BrickStructure txt file
     * @return the BrickStructure read from the specified txt file if the
     * txt file exists. Otherwise, this will return null.
     */
    public BrickStructure readStructure(String path) {
        BrickStructure result = new BrickStructure();
        try {
            result = readStructureFromStream(new FileInputStream(path));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return result;

    }

    public BrickStructure readStructureFromStream(InputStream stream) {
        BrickStructure result = new BrickStructure();
        Scanner fileIn = new Scanner(stream);
        result.setId(Integer.parseInt(fileIn.nextLine()));
        result.setName(fileIn.nextLine());
        int currentRow = -1;
        while (fileIn.hasNext()
                && currentRow <= BrickStructure.MAX_HEIGHT) {
            currentRow++;
            String line = fileIn.nextLine();
            for (int i = 0; i < line.length(); i++) {
                try {
                    int current
                            = Integer.parseInt(line.substring(i, i + 1));

                    Brick brick
                            = new Brick(new Vector2f(i * 64,
                            currentRow * 32));
                    brick.setLevel(BrickStrength.values()[current]);

                    result.setBrick(i, currentRow, brick);
                } catch (NumberFormatException ignored) {}
            }
        }
        return result;
    }
}
