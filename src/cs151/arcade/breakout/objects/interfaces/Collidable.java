package cs151.arcade.breakout.objects.interfaces;

/**
 * Describes a class that is able to collide with another
 * <code>GameObject</code>
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/17/14
 */
public interface Collidable {

    /**
     * Called when this object collides with another GameObject
     */
    void onCollide();

}
