package cs151.arcade.breakout.objects.interfaces;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.state.StateBasedGame;

/**
 * Indicates that something can be rendered to the screen by a Graphics
 * object provided by Slick2D.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/6/14
 */
public interface Renderable {
    /**
     * The method where the rendering of this object should take place.
     *
     * @param container the container holding the game
     * @param game      the game holding this states and that has the ability
     *                  to change game states
     * @param g         the graphics context to render to
     */
    public void render(GameContainer container,
                       StateBasedGame game,
                       Graphics g);
}
