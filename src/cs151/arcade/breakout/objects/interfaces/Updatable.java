package cs151.arcade.breakout.objects.interfaces;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.state.StateBasedGame;

/**
 * Describes a class with the capability to be updated every time the
 * game's update method is called.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/17/14
 */
public interface Updatable {

    /**
     * Updates the logic of this object
     *
     * @param container The game container in charge of managing input and
     *                  handling the game loop
     * @param game      The game in charge of managing the state system
     * @param delta     The amount of milliseconds that occurred from the
     *                  last time that the game's update that was called
     */
    public void update(GameContainer container,
                       StateBasedGame game,
                       int delta);

}
