package cs151.arcade.breakout.particles;

import cs151.arcade.breakout.util.AndRandom;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

import java.util.ArrayList;
import java.util.List;

/**
 * Singleton class used to manage a large amount of particles that can be
 * created in any class.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/31/14
 */
public class ParticleSystem {

    /**
     * Velocity factor representing how fast a default particle moves
     */
    private static final float VEL_FACTOR = 1.0f;

    /**
     * Array list containing a large amount of particles.  An array list
     * was used because removing from the middle of the list happens often.
     */
    private static List<Particle> particles = new ArrayList<Particle>();

    /**
     * Random number generator for stuff
     */
    private static AndRandom rand           = new AndRandom();

    /**
     * Adds particles to the specified position
     *
     * @param pos position to add the particles at
     * @param amount amount of particles to add
     * @param width width of the particles to add
     * @param height height of the particles to add
     * @param intensity how much the speed of the particles should
     *                  increase/decrease from default speed
     * @param duration the amount of time for the particle's to last on
     *                 screen
     */
    public static void addParticles(Vector2f pos,
                                    int amount,
                                    int width,
                                    int height,
                                    float intensity,
                                    int duration) {

        // TODO: Better random color generation
        for (int i = 0; i < amount; i++) {
            Vector2f randVel = getRandCircularVel(VEL_FACTOR * intensity);
            particles.add(new Particle(pos.x, pos.y, width, height,
                    randVel.x, randVel.y, duration,
                    randOrange()));
        }
    }

    /**
     * Adds an explosion to the screen (i.e. a group of particles)
     *
     * @param pos the position to add the explosion at
     */
    public static void addExplosion(Vector2f pos) {
        final int size = 3;
        final int amount = 40;
        final float radius = 10;

        // TODO: Fix this yuck
        addParticles(pos,
                amount,
                size,
                size,
                rand.nextFloat(.2f, 1f),
                rand.nextInt(1000, 1500));
        addParticles(pos,
                amount,
                size,
                size,
                rand.nextFloat(.2f, 1f),
                rand.nextInt(200, 600));
        addParticles(pos.copy().add(new Vector2f(-radius, -radius)),
                amount,
                size,
                size,
                rand.nextFloat(.2f, 1f),
                rand.nextInt(500, 1000));
        addParticles(pos.copy().add(new Vector2f(radius, radius)),
                amount,
                size,
                size,
                rand.nextFloat(.2f, 1f),
                rand.nextInt(500, 1000));
        addParticles(pos.copy().add(new Vector2f(-radius, radius)),
                amount,
                size,
                size,
                rand.nextFloat(.2f, 1f),
                rand.nextInt(500, 1000));
        addParticles(pos.copy().add(new Vector2f(radius, -radius)),
                amount,
                size,
                size,
                rand.nextFloat(.2f, 1f),
                rand.nextInt(500, 1000));
    }

    /**
     * Updates the particles in this particle manager
     *
     * @param container the container holding the game
     * @param game      the game holding this state
     * @param delta     the amount of time that's passed in millisecond since
     *                  last update
     */
    public static void update(GameContainer container,
                              StateBasedGame game,
                              int delta) {
        for (int i = 0; i < particles.size(); i++) {
            Particle p = particles.get(i);
            if (p.isDead()) {
                particles.remove(p);
            } else {
                p.update(container, game, delta);
            }
        }
    }

    /**
     * Renders all the particles in this particle manager
     *
     * @param container the container holding the game
     * @param game      the game holding this states and that has the ability
     *                  to change game states
     * @param g         the graphics context to render to
     */
    public static void render(GameContainer container,
                              StateBasedGame game,
                              Graphics g) {
        for (Particle p : particles) {
            p.render(container, game, g);
        }
    }

    /**
     * Gets a random circular velocity (a velocity around the unit circle)
     *
     * @param v the speed the particles should move outward
     * @return the resulting vector moving in random positions
     */
    private static Vector2f getRandCircularVel(float v) {
        return getRandCircularVel(v, v);
    }

    /**
     * Gets a random velocity on the unit circle
     *
     * @param vX x velocity
     * @param vY y velocity
     * @return a vector velocity applied around the unit circle (a random
     * angle between 0 and 360)
     */
    private static Vector2f getRandCircularVel(float vX,
                                               float vY) {
        float angle = rand.nextInt(360);

        return new Vector2f(vX * (float)Math.cos(angle),
                            vY * (float)Math.sin(angle));
    }

    /**
     * Gets a random shade of orange/red/yellow
     *
     * @return a random shade of orange/red/yellow
     */
    private static Color randOrange() {
        int random = rand.nextInt();
        if (random % 2 == 0) {
            return Color.red;
        } else if (random % 3 == 0) {
            return Color.orange;
        } else {
            return Color.yellow;
        }
    }
}
