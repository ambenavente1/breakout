package cs151.arcade.breakout.sounds;

import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;

/**
 * Created with IntelliJ IDEA.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/18/14
 */
public class SoundManager {

    public static Sound POP;
    public static Music SONG;
    public static Sound BREAK;
    static {
        try {
            POP = new Sound("res/sounds/pop.ogg");
            SONG = new Music("res/sounds/awake! (megawall-10).ogg");
            BREAK = new Sound("res/sounds/jump_1.wav");
        } catch (SlickException e) {
            e.printStackTrace();
        }
    }

}
