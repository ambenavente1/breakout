package cs151.arcade.breakout.states;

import cs151.arcade.breakout.main.Breakout;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

/**
 * TODO: Class description goes here.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/18/14
 */
public class GameOverState extends BasicGameState {

    /**
     * Amount of milliseconds to stay in this mode
     */
    private static final int MAX_TIME = 2500;

    /**
     * The message to display on the screen for this state
     */
    private static final String MESSAGE = "G A M E    O V E R";

    /**
     * Amount of seconds game over has been showed
     */
    private float elapsed;

    /**
     * @see org.newdawn.slick.state.GameState#getID()
     */
    @Override
    public int getID() {
        return States.GAME_OVER.getID();
    }

    /**
     * We do not need to initialize anything for this state
     *
     * @param container The container holding the game
     * @param game      The game holding this state
     * @throws org.newdawn.slick.SlickException Indicates a failure to
     * initialise a resource for this state
     */
    @Override
    public void init(GameContainer container,
                     StateBasedGame game) throws SlickException {
    }

    /**
     * Render this state to the game's graphics context
     *
     * @param container The container holding the game
     * @param game      The game holding this state
     * @param g         The graphics context to render to
     * @throws org.newdawn.slick.SlickException Indicates a failure to render
     * an artifact
     */
    @Override
    public void render(GameContainer container,
                       StateBasedGame game,
                       Graphics g) throws SlickException {
        g.drawString(MESSAGE,
                Breakout.GAME_WIDTH / 2 - g.getFont().getWidth(MESSAGE) / 2,
                Breakout.GAME_HEIGHT / 2 - g.getFont().getLineHeight() / 2);

    }

    /**
     * Update the state's logic based on the amount of time thats passed
     *
     * @param container The container holding the game
     * @param game      The game holding this state
     * @param delta     The amount of time thats passed in millisecond since last update
     * @throws org.newdawn.slick.SlickException Indicates an internal error that will be reported through the
     *                                          standard framework mechanism
     */
    @Override
    public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
        // Game over screen lasts 2.5 seconds or 2500 milliseconds
        if ((elapsed += delta) > MAX_TIME) {
            elapsed = 0;
            game.enterState(States.MAIN_MENU.getID(),
                    new FadeOutTransition(),
                    new FadeInTransition());
        }
    }
}
