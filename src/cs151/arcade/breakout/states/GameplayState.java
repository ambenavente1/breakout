package cs151.arcade.breakout.states;

import cs151.arcade.breakout.objects.Ball;
import cs151.arcade.breakout.objects.Paddle;
import cs151.arcade.breakout.objects.bricks.Brick;
import cs151.arcade.breakout.main.Breakout;
import cs151.arcade.breakout.managers.LevelManager;
import cs151.arcade.breakout.sounds.SoundManager;
import cs151.arcade.breakout.util.AndRandom;
import org.newdawn.slick.*;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

import java.util.List;

/**
 * TODO: Class description goes here
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/17/14
 */
public class GameplayState extends BasicGameState {

    private enum SubState {
        WAITING,
        PLAYING,
        CHANGING
    }

    private static String NOT_STARTED = "Press [SPACE] to start";

    private static int MAX_TIME_CHANGING = 1000;

    /**
     * The manager in charge of updating the levels
     */
    private LevelManager manager;

    /**
     * The player controlled paddle
     */
    private Paddle paddle;

    /**
     * The ball that bounces around the world
     */
    private Ball ball;

    /**
     * Random number generator for this class
     */
    private AndRandom rand;

    /**
     * The color of the back drop
     */
    private Color backgroundColor;

    /**
     * Time spent changing levels
     */
    private int timeChanging;

    /**
     * Current sub state of this state
     */
    private SubState currentSubState;

    /**
     * Number of levels beat
     */
    private int levelsBeat;

    /**
     * @see org.newdawn.slick.state.GameState#getID()
     */
    @Override
    public int getID() {
        return States.GAME_PLAY.getID();
    }

    /**
     * Initialise the state. It should load any resources it needs at this
     * stage
     *
     * @param container The container holding the game
     * @param game      The game holding this state
     * @throws org.newdawn.slick.SlickException Indicates a failure to
     * initialise a resource for this state
     */
    @Override
    public void init(GameContainer container,
                     StateBasedGame game) throws SlickException {
        manager = new LevelManager();
        paddle = new Paddle();
        ball = new Ball(paddle);
        rand = new AndRandom();
        backgroundColor = new Color(0x4A255C);
        levelsBeat = 0;
        currentSubState = SubState.WAITING;
    }

    /**
     * Render this state to the game's graphics context
     *
     * @param container The container holding the game
     * @param game      The game holding this state
     * @param g         The graphics context to render to
     * @throws org.newdawn.slick.SlickException Indicates a failure to render
     * an artifact
     */
    @Override
    public void render(GameContainer container,
                       StateBasedGame game,
                       Graphics g) throws SlickException {
        g.setColor(backgroundColor);
        g.fillRect(0, 0, Breakout.GAME_WIDTH, Breakout.GAME_HEIGHT);
        g.setColor(Color.white);

        if (currentSubState == SubState.WAITING) {
            Font gFont = g.getFont();
            g.drawString(NOT_STARTED,
                         Breakout.GAME_WIDTH / 2 - gFont.getWidth
                                 (NOT_STARTED) / 2,
                         Breakout.GAME_HEIGHT / 2 - gFont.getLineHeight() /
                                 2);
        } else if (currentSubState == SubState.CHANGING) {
            Font gFont = g.getFont();
            g.drawString(manager.getCurrentLevel().getName(),
                    Breakout.GAME_WIDTH / 2 - gFont.getWidth
                            (manager.getCurrentLevel().getName()) / 2,
                    Breakout.GAME_HEIGHT / 2 - gFont.getLineHeight() /
                            2 );
        }

        if (!manager.isGameOver()) {
            manager.getCurrentLevel().render(container, game, g);

            if (!paddle.isGameOver()) {
                paddle.render(container, game, g);
                ball.render(container, game, g);
            }
        }
    }

    /**
     * Update the state's logic based on the amount of time that's passed
     *
     * @param container The container holding the game
     * @param game      The game holding this state
     * @param delta     The amount of time that's passed in millisecond since
     *                  last update
     * @throws org.newdawn.slick.SlickException Indicates an internal error
     * that will be reported through the standard framework mechanism
     */
    @Override
    public void update(GameContainer container,
                       StateBasedGame game,
                       int delta) throws SlickException {
        SoundManager.SONG.setVolume(0.3f);

        if (currentSubState != SubState.CHANGING) {
            paddle.update(container, game, delta);
        } else {
            if ((timeChanging += delta) >= MAX_TIME_CHANGING) {
                timeChanging = 0;
                currentSubState = SubState.WAITING;
            }
        }

        if (currentSubState == SubState.PLAYING) {
            if (manager.isEmpty()) {
                manager.nextLevel();
                ball.resetPos(paddle);
                currentSubState = SubState.CHANGING;
                if (++levelsBeat % 2 == 0) {
                    paddle.incrementLife();
                }
            }

            ball.update(container, game, delta);

            if (ball.getY() > Breakout.GAME_HEIGHT) {
                paddle.deductLife();
                ball.resetPos(paddle);
                currentSubState = SubState.WAITING;
            }

            checkForPlayerGameOver(container, game);

            checkPaddleBallCollision();
            checkBallBrickCollisions();

            checkForLevelManagerGameOver(container, game);
        }

        if (currentSubState == SubState.WAITING) {
            if (container.getInput().isKeyPressed(Input.KEY_SPACE)) {
                currentSubState = SubState.PLAYING;
            }
            ball.resetPos(paddle);
        }
    }

    /**
     * Checks if the LevelManager has announced game over because the
     * player passed the final level
     *
     * @param container The container holding the game
     * @param game the game in charge of switching the game state
     */
    private void checkForLevelManagerGameOver(GameContainer container,
                                              StateBasedGame game) {
        if (manager.isGameOver()) {
            enterGameOver(container, game);
        }
    }

    /**
     * Checks if the Paddle has announced game over because it ran out of
     * lives
     *
     * @param container The container holding the game
     * @param game the game in charge of switching the game state
     */
    private void checkForPlayerGameOver(GameContainer container,
                                        StateBasedGame game) {
        if (paddle.isGameOver()) {
            enterGameOver(container, game);
        }
    }

    @Override
    public void enter(GameContainer container,
                      StateBasedGame game) throws SlickException {
        init(container, game);
    }

    /**
     * Enters the game over state
     *
     * @param container the container holding the game
     * @param game      the game in charge of switching the game state
     */
    private void enterGameOver(GameContainer container,
                               StateBasedGame game) {
        game.enterState(States.GAME_OVER.getID(),
            new FadeOutTransition(),
            new FadeInTransition());
    }

    /**
     * Checks if the ball has collided with any bricks
     */
    private void checkBallBrickCollisions() {
        List<Brick> bricksHit = manager.getCurrentLevel().collision(ball);
        if (bricksHit.size() > 0) {
            Brick sampleBrick = bricksHit.get(rand.nextInt(bricksHit.size()));

            if ((ball.getY() + ball.getHeight()) <= sampleBrick.getY()
                    - (sampleBrick.getHeight() / 2) ||
                ball.getY() >= sampleBrick.getY() + sampleBrick.getHeight()
                 / 2   ) {
                // Ball was hit from the top
                ball.reflectY();
                sampleBrick.onCollide();
            } else {
                // Ball was hit from the right
                ball.reflectX();
                sampleBrick.onCollide();
            }

        }
    }

    /**
     * Checks if the paddle intersects the ball
     */
    private void checkPaddleBallCollision() {
        if (ball.getVel().y > 0) {
            if (ball.getBounds().intersects(paddle.getBounds())) {
                if (ball.getY() > paddle.getY()) {
                    // Too far
                    ball.reflectX();
                } else {
                    ball.reflectY();
                }
                ball.addVel(new Vector2f(paddle.getVel().x, 0));
            }
        }
    }

}
