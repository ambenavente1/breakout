package cs151.arcade.breakout.states;

import cs151.arcade.breakout.objects.TitleBall;
import cs151.arcade.breakout.main.Breakout;
import cs151.arcade.breakout.particles.Particle;
import cs151.arcade.breakout.particles.ParticleSystem;
import cs151.arcade.breakout.sounds.SoundManager;
import cs151.arcade.breakout.ui.Control;
import cs151.arcade.breakout.ui.ControlManager;
import cs151.arcade.breakout.ui.Label;
import cs151.arcade.breakout.ui.LinkLabel;
import cs151.arcade.breakout.ui.events.ActionArgs;
import cs151.arcade.breakout.ui.events.ActionDoer;
import org.newdawn.slick.*;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import java.awt.Font;

import org.newdawn.slick.state.transition.FadeInTransition;
import org.newdawn.slick.state.transition.FadeOutTransition;

/**
 * TODO: Class description goes here
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/17/14
 */
public class MainMenuState extends BasicGameState {

    private ControlManager controlManager;
    private StateBasedGame parentGame;
    private TitleBall titleBall;
    private Color backgroundColor;

    public MainMenuState(StateBasedGame parentGame) {
        this.parentGame = parentGame;
    }

    /**
     * @see org.newdawn.slick.state.GameState#getID()
     */
    @Override
    public int getID() {
        return States.MAIN_MENU.getID();
    }

    /**
     * Initialise the state. It should load any resources it needs at this
     * stage
     *
     * @param container The container holding the game
     * @param game      The game holding this state
     * @throws org.newdawn.slick.SlickException Indicates a failure to
     * initialise a resource for this state
     */
    @Override
    public void init(GameContainer container,
                     StateBasedGame game) throws SlickException {
        controlManager = new ControlManager();
        titleBall = new TitleBall(new Vector2f(0, 0));
        titleBall.setRandPos();

        TrueTypeFont titleFont = new TrueTypeFont(new Font("Myriad-Pro " +
                "Regular", Font.PLAIN, 48), true);
        TrueTypeFont labelFont = new TrueTypeFont(new Font("Myriad-Pro " +
                "Regular", Font.PLAIN, 20), true);
        final int LABEL_PADDING = 25;

        Control title = new Label("lblTitle", "Breakout!");
        title.setFont(titleFont);
        title.setWidth(titleFont.getWidth(title.getText()));
        title.setHeight(titleFont.getLineHeight());
        title.setPos(new Vector2f(Breakout.GAME_WIDTH / 2
                - title.getWidth() / 2, 90));
        controlManager.add(title);

        Vector2f startPos = title.getPos().copy();
        startPos.y += title.getHeight() + LABEL_PADDING * 1.75f;

        LinkLabel lblPlay = new LinkLabel("lblPlay",
                "Play");
        lblPlay.setFont(labelFont);
        lblPlay.setWidth(labelFont.getWidth(lblPlay.getText()));
        lblPlay.setHeight(labelFont.getLineHeight());
        lblPlay.setActionDoer(new ActionDoer() {
            @Override
            public void doAction(ActionArgs e) {
                parentGame.enterState(States.GAME_PLAY.getID(),
                        new FadeOutTransition(),
                        new FadeInTransition());
            }
        });
        startPos.x = Breakout.GAME_WIDTH / 2
                - lblPlay.getWidth() / 2;
        lblPlay.setPos(startPos.copy());
        lblPlay.setHighlightColor(Color.blue);
        controlManager.add(lblPlay);

        startPos.y += lblPlay.getHeight() + LABEL_PADDING;

        LinkLabel lblQuit = new LinkLabel("lblQuit", "Quit");
        lblQuit.setFont(labelFont);
        lblQuit.setWidth(labelFont.getWidth(lblQuit.getText()));
        lblQuit.setHeight(labelFont.getHeight());
        lblQuit.setActionDoer(new ActionDoer() {
            @Override
            public void doAction(ActionArgs e) {
                parentGame.enterState(States.QUIT.getID(),
                        new FadeOutTransition(),
                        new FadeInTransition());
            }
        });
        startPos.x = Breakout.GAME_WIDTH / 2 - lblQuit.getWidth() / 2;
        lblQuit.setPos(startPos.copy());
        lblQuit.setHighlightColor(Color.blue);
        controlManager.add(lblQuit);

        backgroundColor = new Color(0x4A255C);

        SoundManager.SONG.loop(1.0f, 1.0f);
    }

    /**
     * Render this state to the game's graphics context
     *
     * @param container The container holding the game
     * @param game      The game holding this state
     * @param g         The graphics context to render to
     * @throws org.newdawn.slick.SlickException Indicates a failure to render
     * an artifact
     */
    @Override
    public void render(GameContainer container,
                       StateBasedGame game,
                       Graphics g) throws SlickException {
        Color originalColor = g.getColor();
        g.setColor(backgroundColor);
        g.fillRect(0, 0, 640, 480);
        g.setColor(originalColor);
        titleBall.render(container, game, g);
        controlManager.render(container, game, g);
        String names = "Anthony B + Daniel P";
        g.drawString(names, Breakout.GAME_WIDTH - g.getFont().getWidth
                        (names),
                Breakout.GAME_HEIGHT - g.getFont().getLineHeight());
        ParticleSystem.render(container, game, g);
    }

    /**
     * Update the state's logic based on the amount of time thats passed
     *
     * @param container The container holding the game
     * @param game      The game holding this state
     * @param delta     The amount of time thats passed in millisecond since
     *                  last update
     * @throws org.newdawn.slick.SlickException Indicates an internal error
     * that will be reported through the standard framework mechanism
     */
    @Override
    public void update(GameContainer container,
                       StateBasedGame game,
                       int delta) throws SlickException {
        ParticleSystem.update(container, game, delta);
        SoundManager.SONG.setVolume(1.0f);
        titleBall.update(container, game, delta);
        controlManager.update(container, game, delta);

        if (container.getInput().isMousePressed(Input.MOUSE_LEFT_BUTTON)) {
            Rectangle estimatedMouse = new Rectangle(0, 0, 0, 0);
            estimatedMouse.setWidth(20);
            estimatedMouse.setHeight(20);
            estimatedMouse.setCenterX(container.getInput().getMouseX());
            estimatedMouse.setCenterY(container.getInput().getMouseY());

            if (estimatedMouse.intersects(titleBall.getBounds())) {
                ParticleSystem.addExplosion(titleBall.centerPos());
                titleBall.setRandPos();
            }
        }
    }
}
