package cs151.arcade.breakout.states;

/**
 * This enumeration contains a list of all the game states in the game.
 * Each element in the enumeration also has an ID that is used by the
 * StateBasedGame to switch to that game state.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/17/14
 */
public enum States {

    /**
     * The splash screen
     */
    SPLASH(0),

    /**
     * The main menu
     */
    MAIN_MENU(1),

    /**
     * Where the actual game play occurs
     */
    GAME_PLAY(2),

    /**
     * Where the game goes to end
     */
    GAME_OVER(3),

    /**
     * State to clean up before exiting
     */
    QUIT(4);

    /**
     * The ID for this state. This is used by the state engine to switch to
     * this state.
     */
    private final int ID;

    /**
     * Initialize a State with a given ID
     *
     * @param ID the ID of the state to add
     */
    private States(int ID) {
        this.ID = ID;
    }

    /**
     * Gets the ID for this state used when switching/entering states
     *
     * @return the ID of this state
     */
    public int getID() {
        return ID;
    }
}
