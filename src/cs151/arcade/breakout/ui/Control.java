/*
 * Copyright (c) 2014 Anthony Benavente
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cs151.arcade.breakout.ui;

import cs151.arcade.breakout.objects.interfaces.Renderable;
import cs151.arcade.breakout.objects.interfaces.Updatable;
import cs151.arcade.breakout.ui.events.ActionDoer;
import cs151.arcade.breakout.ui.events.MouseEventArgs;
import cs151.arcade.breakout.ui.events.MouseEventListener;
import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

/**
 * A control is something that is on the screen that represents gui
 * component.  Control's can typically be interacted with or display some
 * kind of data to the user.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 2/19/14
 */
public abstract class Control implements Renderable,
        Updatable, MouseEventListener {

    /**
     * The name of the control for identification
     */
    private String name;

    /**
     * The text displayed by this control
     */
    private String text;

    /**
     * Where the control is located
     */
    private Vector2f pos;

    /**
     * The width of this control
     */
    private int width;

    /**
     * The height of this control
     */
    private int height;

    /**
     * The value that this control holds.  This can be anything.
     */
    private Object value;

    /**
     * If this control is in focus
     */
    private boolean hasFocus;

    /**
     * If the control is enabled or not
     */
    private boolean isEnabled;

    /**
     * If the control is visible when rendered
     */
    private boolean isVisible;

    /**
     * The font used to draw text in this control
     */
    private Font font;

    /**
     * The color of the font for this control
     */
    private Color foreColor;

    /**
     * The background color of this control
     */
    private Color backColor;

    /**
     * The rectangular collision area for this control
     */
    private Rectangle bounds;

    /**
     * The event that is called when this control is interacted with.  This
     * is specified by the user
     */
    protected ActionDoer doer;

    /**
     * Creates a default control with no name or values
     */
    public Control() {
        this("");
    }

    /**
     * Creates a control with a name for identification
     *
     * @param name the name of this control to be used for identification
     *             purposes
     */
    public Control(String name) {
        this(name, new Vector2f(0, 0));
    }

    /**
     * Creates a control with a name and a starting position
     *
     * @param name the name of this control to be used for identification
     *             purposes
     * @param pos  the position of the control
     */
    public Control(String name, Vector2f pos) {
        this(name, pos, 0, 0);
    }

    /**
     * Creates a control with a specified position, name, width, and height
     *
     * @param name   the name of this control to be used for identification
     *               purposes
     * @param pos    the position of the control
     * @param width  the width of this control
     * @param height the height of this control
     */
    public Control(String name,
                   Vector2f pos,
                   int width,
                   int height) {
        this.name = name;
        this.pos = pos;
        this.width = width;
        this.height = height;
        this.text = "";
        this.value = null;
        this.hasFocus = false;
        this.isEnabled = true;
        this.isVisible = true;
        this.font = null;
        this.foreColor = Color.white;
        this.backColor = Color.black;
        this.bounds = new Rectangle(pos.x, pos.y, width, height);
    }

    /**
     * Updates the control
     *
     * @param container the container holding the game
     * @param game      the game holding this state
     * @param delta     the amount of time that's passed in millisecond since
     */
    @Override
    public abstract void update(GameContainer container,
                                StateBasedGame game,
                                int delta);

    /**
     * Renders the control
     *
     * @param container the container holding the game
     * @param game      the game holding this states and that has the ability
     *                  to change game states
     * @param g         the graphics context to render to
     */
    @Override
    public void render(GameContainer container,
                       StateBasedGame game,
                       Graphics g) {
    }

    /**
     * Called when the control is clicked on
     *
     * @param e the mouse arguments at the time of the event call
     */
    @Override
    public void onClick(MouseEventArgs e) {
    }

    /**
     * Called when the control is hovered over
     *
     * @param e the mouse arguments at the time of the event call
     */
    @Override
    public void onHover(MouseEventArgs e) {
    }

    /**
     * Called when the mouse leaves the control.  The control at this point
     * loses focus from the manager
     *
     * @param e the mouse arguments at the time of the event call
     */
    @Override
    public void onMouseLeave(MouseEventArgs e) {
        if (hasFocus) hasFocus = false;
    }

    /**
     * Called when the mouse enters the control.  The control,
     * at this point, become the center of focus for the control manager
     *
     * @param e the mouse arguments at the time of the event call
     */
    @Override
    public void onMouseEnter(MouseEventArgs e) {
        if (!hasFocus) hasFocus = true;
    }

    /**
     * Called when the mouse has held the button down over the control.
     *
     * @param e the mouse arguments at the time of the event call
     */
    @Override
    public void onMouseDown(MouseEventArgs e) {
    }

    /**
     * Gets the name or the identification for this control
     *
     * @return the name of this control
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name or the identification of this control
     *
     * @param name the new name of this control
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the text displayed by this control if it has that ability.
     *
     * @return the text of this control
     */
    public String getText() {
        return text;
    }

    /**
     * Sets the text to be displayed by this control if possible
     *
     * @param text the text to be displayed by this control
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * Gets the position of this control on screen
     *
     * @return the position of the control
     */
    public Vector2f getPos() {
        return pos;
    }

    /**
     * Sets the position of the control
     *
     * @param pos the new position of the control
     */
    public void setPos(Vector2f pos) {
        this.pos = pos;
        updateBounds();
    }

    /**
     * Gets the x component of this controls position
     *
     * @return the x coordinate of this control
     */
    public float getX() {
        return pos.x;
    }

    /**
     * Sets the x component of this control's position
     *
     * @param x the new x coordinate of this control's position
     */
    public void setX(float x) {
        pos.x = x;
        updateBounds();
    }

    /**
     * Gets the y component of this control's position
     *
     * @return the new y coordinate of this control's position
     */
    public float getY() {
        return pos.y;
    }

    /**
     * Sets the y component of this control's position
     *
     * @param y the new y coordinate of this control's position
     */
    public void setY(float y) {
        pos.y = y;
        updateBounds();
    }

    /**
     * Gets the width of this control
     *
     * @return the width of this control
     */
    public int getWidth() {
        return width;
    }

    /**
     * Sets the width of this control
     *
     * @param width the new width of this control
     */
    public void setWidth(int width) {
        this.width = width;
        updateBounds();
    }

    /**
     * Gets the height of this control
     *
     * @return the height of this control
     */
    public int getHeight() {
        return height;
    }

    /**
     * Sets the height of this control
     *
     * @param height the new height of this control
     */
    public void setHeight(int height) {
        this.height = height;
        updateBounds();
    }

    /**
     * Gets the value held by this control (optional)
     *
     * @return this control's value
     */
    public Object getValue() {
        return value;
    }

    /**
     * Sets the value held by this control
     *
     * @param value the new value to be held by this control
     */
    public void setValue(Object value) {
        this.value = value;
    }

    /**
     * Gets if this control is in focus
     *
     * @return if this control is in focus
     */
    public boolean isHasFocus() {
        return hasFocus;
    }

    /**
     * Sets if this control has focus
     *
     * @param hasFocus if this control has focus
     */
    public void setHasFocus(boolean hasFocus) {
        this.hasFocus = hasFocus;
    }

    /**
     * Gets if this control is enabled or not
     *
     * @return if this control is enabled
     */
    public boolean isEnabled() {
        return isEnabled;
    }

    /**
     * Sets if this control is enabled
     *
     * @param isEnabled if the control is enabled
     */
    public void setEnabled(boolean isEnabled) {
        this.isEnabled = isEnabled;
    }

    /**
     * If the control is visible when drawn
     *
     * @return if the control is visible
     */
    public boolean isVisible() {
        return isVisible;
    }

    /**
     * Sets if the control is visible
     *
     * @param isVisible if the control is visible
     */
    public void setVisible(boolean isVisible) {
        this.isVisible = isVisible;
    }

    /**
     * Gets the font used when rendering text for this control
     *
     * @return this control's font
     */
    public Font getFont() {
        return font;
    }

    /**
     * Sets the font used when rendering text for this control
     *
     * @param font the new font for this control
     */
    public void setFont(Font font) {
        this.font = font;
    }

    /**
     * Gets the color of the font for this control
     *
     * @return the color for the text
     */
    public Color getForeColor() {
        return foreColor;
    }

    /**
     * Sets the color of the font for this control
     *
     * @param foreColor the new color for the font of this control
     */
    public void setForeColor(Color foreColor) {
        this.foreColor = foreColor;
    }

    /**
     * Gets the background color of this control
     *
     * @return the background color of this control
     */
    public Color getBackColor() {
        return backColor;
    }

    /**
     * Sets the background color of the control
     *
     * @param backColor the new background color of the control
     */
    public void setBackColor(Color backColor) {
        this.backColor = backColor;
    }

    /**
     * Gets the bounding box for the control for use with collisions
     *
     * @return the bounding box for this control
     */
    public Rectangle getBounds() {
        return bounds;
    }

    /**
     * Updates the bounds when position or dimensions change
     */
    private void updateBounds() {
        bounds.setBounds(pos.x, pos.y, width, height);
    }

    /**
     * Sets what happens when this control is interacted with
     *
     * @param doer the event that should happen when called on this event
     */
    public void setActionDoer(ActionDoer doer) {
        this.doer = doer;
    }

    /**
     * Checks if the mouse is inside of the control's bounds
     *
     * @param mousePos the position of the mouse
     * @return if the mouse's position is inside the control's bounds
     */
    protected boolean mouseInBounds(Vector2f mousePos) {
        return bounds.contains(mousePos.x, mousePos.y);
    }
}
