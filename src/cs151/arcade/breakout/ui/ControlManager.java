package cs151.arcade.breakout.ui;

import cs151.arcade.breakout.objects.interfaces.Renderable;
import cs151.arcade.breakout.objects.interfaces.Updatable;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.state.StateBasedGame;

import java.util.ArrayList;
import java.util.List;

/**
 * Manages a group of controls for the gui.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/20/14
 */
public class ControlManager implements Renderable, Updatable {

    /**
     * The list of controls
     */
    private List<Control> controls;

    /**
     * Creates a default control manager with a default initial capacity of
     * 10
     */
    public ControlManager() {
        controls = new ArrayList<Control>();
    }

    /**
     * Creates a control manager with a starting capacity
     *
     * @param maxSize the initial capacity for this control
     */
    public ControlManager(int maxSize) {
        controls = new ArrayList<Control>(maxSize);
    }

    /**
     * Renders all the controls
     *
     * @param container the container holding the game
     * @param game      the game holding this states and that has the ability
     *                  to change game states
     * @param g         the graphics context to render to
     */
    @Override
    public void render(GameContainer container,
                       StateBasedGame game,
                       Graphics g) {
        for (int i = controls.size() - 1; i >= 0; i--) {
            controls.get(i).render(container, game, g);
        }
    }

    /**
     * Updates all the controls
     *
     * @param container the container holding the game
     * @param game      the game holding this state
     * @param delta     the amount of time that's passed in millisecond since
     */
    @Override
    public void update(GameContainer container,
                       StateBasedGame game,
                       int delta) {
        for (int i = controls.size() - 1; i >= 0; i--) {
            controls.get(i).update(container, game, delta);
        }
    }

    /**
     * Adds a control to the list of controls
     *
     * @param c the control to add
     */
    public void add(Control c) {
        controls.add(c);
    }

    /**
     * Removes the specified control
     *
     * @param c the control to remove
     */
    public void remove(Control c) {
        if (controls.contains(c)) {
            remove(controls.indexOf(c));
        }
    }

    /**
     * Removes the control at the specified index
     *
     * @param index the index of the control to remove
     */
    public void remove(int index) {
        if (index >= 0 && index < controls.size()) {
            controls.remove(index);
        }
    }
}
