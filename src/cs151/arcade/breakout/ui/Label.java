/*
 * Copyright (c) 2014 Anthony Benavente
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cs151.arcade.breakout.ui;

import org.newdawn.slick.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

/**
 * A Label is a control whose only purpose is to display text onto the screen.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 2/19/14
 */
public class Label extends Control {

    /**
     * Last position of the mouse from the last update
     */
    private Vector2f lastMousePos;

    /**
     * Current position of the mouse from this update
     */
    private Vector2f currMousePos;

    /**
     * Creates a new Label
     *
     * @param title name/identification for the label
     * @param text  the text to be displayed by this label
     */
    public Label(String title, String text) {
        super(title);

        setText(text);
        lastMousePos = new Vector2f(0, 0);
        currMousePos = new Vector2f(0, 0);
    }

    /**
     * Updates the label (doesn't really do anything but update the mouse's
     * positions)
     *
     * @param container the container holding the game
     * @param game      the game holding this state
     * @param delta     the amount of time that's passed in millisecond since
     */
    @Override
    public void update(GameContainer container,
                       StateBasedGame game,
                       int delta) {
        Input input = container.getInput();

        // Do stuff
        currMousePos.set(input.getMouseX(), input.getMouseY());

        // Set the last pos last
        lastMousePos.set(input.getMouseX(), input.getMouseY());
    }

    /**
     * Renders the label to the screen
     *
     * @param container the container holding the game
     * @param game      the game holding this states and that has the ability
     *                  to change game states
     * @param g         the graphics context to render to
     */
    @Override
    public void render(GameContainer container,
                       StateBasedGame game,
                       Graphics g) {
        Font tmpFont = g.getFont();
        setWidth(tmpFont.getWidth(getText()));
        setHeight(tmpFont.getHeight(getText()));
        if (getFont() != null) {
            g.setFont(getFont());
            setWidth(getFont().getWidth(getText()));
            setHeight(getFont().getHeight(getText()));
        }
        g.drawString(getText(), getX(), getY());
        g.setFont(tmpFont);
    }
}
