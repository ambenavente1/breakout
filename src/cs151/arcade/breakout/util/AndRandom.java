package cs151.arcade.breakout.util;

import java.util.Random;

/**
 * Random generator with the ability to get random numbers from within a
 * range
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 3/18/14
 */
public class AndRandom extends Random {

    /**
     * Gets a random int within the interval [min, max)
     *
     * @param min the lowest value including this one
     * @param max the highest value excluding this one
     * @return a random int within the interval [min, max)
     */
    public int nextInt(int min, int max) {
        return nextInt(max - min) + min;
    }

    /**
     * Gets a random float within the interval [min, max)
     *
     * @param min the lowest value including this one
     * @param max the highest value excluding this one
     * @return a random float within the interval [min, max)
     */
    public float nextFloat(float min, float max) {
        return (nextFloat() * max) + min;
    }
}
